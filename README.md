# Portfolio Website


## Introduction 
This project is a portfolio website for Ulysse Hellouin.

## Requirements
Node.js : https://nodejs.org/en/download/  
Node version : 8.9.4

## Run instruction
1. Clone the repository
2. run ```npm install```to install the dependencies
3. run ```npm run populate``` to populate the database
4. run ```npm run test```to run the unit tests
5. run ```npm start```to launch the application
6. run ```npm run dev```to launch the application in dev mode

## Contributors
Mattias Hellouin
Maximilien Sock
Tao Yang

