

exports.login_post = function(err,result) {

  //return dbUser.get(req.body.username, (err, result))
};


exports.project = function(id){
  const project = [{
    title:"FENG CHIA NIGHT MARKET RESEARCH TEAM",
    date:"Sep 2018 - Dec 2018",
    desc:"The Feng Chia Night Market is one of Taichung’s main sites of commerce. It has suffered from a recent reduction in area tourism as a result of increasing political tensions with mainland China. Feng Chia University created a research group to analyze the situation and propose new insights and solutions. The results of this research in the form of a printed book were presented to both the dean of the university and the head of the night market.",
    client:"Feng Chia University, Taiwan",
    type:"Research & Proposal",
    role:"Resource investigator, coordinator, environmental science specialist",
    subDate:"December 2018",
    team:"I was among five architecture students; our professor assisted as well.",
    min:"/images/Miniature1.jpg",
    pdf:"/images/FengjiaNightMarket.pdf"
  },
  {
    title:"Moon Lamp",
    date:"Nov 2018 - Dec 2018",
    desc:"The final exam of the Green Design Course was the creation of a lamp using only green products.  This required a prototype, 3D model, and final execution.",
    client:"Green Design, Feng Chia University, Taiwan",
    type:"Final Exam",
    role:"Independent work with assistance",
    subDate:"December 2018",
    team:"N/A",
    min:"/images/lamp_min.jpg",
    pdf:"/images/MoonLampConcept.pdf"
  }]
  return project[id]
};

exports.steps = function(id){
  const steps = [10]
   steps[0] = [
  {
    title:"Empathize",
    details:"We interviewed various shop owners and vendors,read academic publications about similar places,performed case study analyses,and conducted field investigations."
  },
  {
    title:"Define",
    details:"We held group discussions with the team, redefined the area to a smaller zone, and defined major points in need of amelioration: lack of parking availability, pollution, and poor user experience."
  },
  {
    title:"Ideate",
    details:"I proposed a solution for the pollution problem: to improve already existing systems (plants to green walls). I also proposed a new way to depollute the city center with algae farming by using the biomass produced for alimentary purposes."
  },
  {
    title:"Prototype",
    details:"I developed a book that presented all our work and organized it. The book is a compilation of interconnected files that can also be read independently. It contains five categories: Analysis, Strategies, Proposal, Buildings, and Food."
  },
  {
    title:"Test",
    details:"We presented our work to the dean and to the head of the night market. Both agreed to fund the team for another semester and gave us a budget for further work!"
  }
]
steps[1] = [
  {
    title:"DEFINE",
    details:"I decided to use myself as the target demographic.  Due to time constraints, I focused on geometric shapes that are easier to design and build.  This desk lamp is battery-powered and removes the need for wires: it is therefore moveable in every direction, which shapes the type of light it emits depending upon its angle, and it also can exist anywhere without needing an external power source.  I decided to use no more than three materials maximum.  I also concluded that the shape of the object should somewhat determine its function."
  },
  {
    title:"IDEATE pt. 1",
    details: "After testing shapes with paper, I soon landed on the structure: an empty skeleton of a cube with the lamp in a container inside."
  },
  {
    title:"PROTOTYPE pt. 1",
    details: "I created an initial prototype with light wood cut in squares."
  },
  {
    title:"TEST pt. 1",
    details: "I presented this prototype to a student panel and noted their feedback."
  },
  {
    title:"IDEATE pt. 2",
    details: "The student panel didn’t like the “box shape” inside but validated the overall structure, including the way in which the user can change the light directionality/strength by repositioning the lamp.  In response to the panel’s critique of the interior box-shaped piece, I used CAD software to create different 3D models amended to incorporate their feedback. Based upon this, I chose materials for the final prototype: concrete, paper, and recycled metal."
  },
  {
    title:"PROTOTYPE pt. 2",
    details: "Using CAD software, I drew up plans for precision cutting.  I then created a second prototype (with wood, paper and recycled metal) of the final design (a sphere inside the cubic structure).  This prototype fully worked with all the precision cuts and screw position.  I then conducted the first tests for a concrete bowl. The concrete bowl unfortunately was too thin and broke, but otherwise, the remainder of the components functioned perfectly."
  },
  {
    title:"TEST pt. 2",
    details: "I presented this second prototype to the teacher, who fully validated the work."
  },
  {
    title:"IDEATE pt. 3",
    details: "I designed a mold for the spherical component and 3D printed it to have a thicker bowl.  I then removed some unnecessary screws"
  },
  {
    title:"PROTOTYPE pt. 3",
    details: "I created a final prototype, this time involving the concrete bowl.  I also illustrated a mockup to show the lamp in its intended environment."
  },
]
  return steps[id]
};

exports.statement = function(id){
  const statement = ["Working alongside a team of architects was a very interesting experience. Although I was not as well-versed in architecture as my peers, I found my place in the group as a coordinator of their work. Additionally, my background in the sciences provided valuable insight in that domain.",
"It was incredibly challenging to create this lamp in such a short time frame, but I learned a lot in the process.  I worked with a variety of techniques and software, some for the first time.  I often had to quickly determine which was most effective because I didn’t have much time to test my ideas."]
  return statement[id]
};

exports.carousel = function(id){
  const carousel = [10]
  carousel[0] = ["/images/NightMarket.jpg"]
  carousel[1] = ["/images/Lamp.jpeg","/images/Lamp2.jpeg","/images/Lamp3.jpeg","/images/Lamp4.jpeg","/images/Lamp5.jpeg"]
  return carousel[id]
};
