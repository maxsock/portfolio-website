import express = require('express')
import bodyparser = require('body-parser')
import morgan = require('morgan')
import session = require('express-session')
import levelSession = require('level-session-store')
import { UserHandler, User } from './user'
import path = require('path')
var moment = require('moment');
import bootstrap = require('bootstrap')
import Instafeed = require("instafeed.js");

const LevelStore = levelSession(session)
const dbUser: UserHandler = new UserHandler('./db/users')


const app = express()
const port: string = process.env.PORT || '8080'



app.use(bodyparser.json())
app.use(bodyparser.urlencoded())
app.use(morgan('dev'))
app.use(session({
  secret: 'my very secret phrase',
  store: new LevelStore('./db/sessions'),
  resave: true,
  saveUninitialized: true
}))

app.set('views', __dirname + "/../views")
app.set('view engine', 'ejs');

app.use('/', express.static(path.join(__dirname,'/../node_modules/jquery/dist')))
app.use('/', express.static(path.join(__dirname,'/../node_modules/bootstrap/dist')))
app.use('/', express.static(path.join(__dirname,'/../node_modules/instafeed.js')))
app.use(express.static('public'))


/*
  Authentication
*/

var authRouter = require('../routes/authentication')
var indexRouter = require('../routes/index')

app.use('/', indexRouter);
app.use(authRouter)


/*
  Root
*/

app.use(function (req: any, res: any, next: any) {
  console.log(req.method + ' on ' + req.url)
  next()
})


app.use(function (err: Error, req: any, res: any, next: any) {
  console.log('got an error')
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.listen(port, (err: Error) => {
  if (err) throw err
  console.log(`server is listening on port ${port}`)
})
