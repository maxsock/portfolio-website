var home_model = require('../models/user');


exports.home = function(req,res,next) {
    res.render("home")
};

exports.projects = function(req,res,next) {
    res.render("projects")
};

exports.contact = function(req,res,next) {
    res.render("contact")
};

exports.instagram = function(req,res,next) {
    res.render("instagram")
};

exports.travelgram = function(req,res,next) {
    res.render("travelgram")
};

exports.project = function(req,res,next) {

  project = home_model.project(req.params.id)
  steps = home_model.steps(req.params.id)
  statement = home_model.statement(req.params.id)
  carousel = home_model.carousel(req.params.id)
  res.render("project",{project,steps,statement,carousel})

}
