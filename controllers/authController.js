
var User = require('../models/user');
var UserHandler = require ('../src/user')

exports.login = function(req,res) {
    res.render("login")
};

exports.login_post = function(req,res,next) {
  const dbUser = new UserHandler.UserHandler('./db/users')

  dbUser.get(req.body.username, (err, result) => {
    if (result === undefined || !result.validatePassword(req.body.password)) {
      res.redirect('/login')
    } else {
      req.session.loggedIn = true
      req.session.user = result
      res.redirect('/')
    }
  })


};
exports.logout = function(req,res) {
  if(req.session.loggedIn){
    delete req.session.loggedIn
    delete req.session.user
  }
  res.redirect('/login')
};
