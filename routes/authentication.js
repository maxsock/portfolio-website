var express = require('express');
var router = express.Router();

var auth_controller = require('../controllers/authController');


router.get('/login', auth_controller.login);

router.post('/login', auth_controller.login_post)

router.get('/logout', auth_controller.logout);

module.exports = router;
