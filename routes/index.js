var express = require('express');
var router = express.Router();

var home_controller = require('../controllers/homeController');


router.get('/', home_controller.home);
router.get('/projects', home_controller.projects);
router.get('/contact', home_controller.contact);
router.get('/instagram', home_controller.instagram);
router.get('/travelgram', home_controller.travelgram);

// projects pages
router.get('/projects/:id',home_controller.project)

module.exports = router;
